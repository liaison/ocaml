(*
 *  Author: Lisong Guo <lisong.guo@inria.fr>
 *  Date:   Nov 8, 2013 
 *  
 *  The algorithm to measure the Levenshtein distance between two strings
 *)



let rec levenshtein_distance s t = 
    let len_s = String.length s and 
        len_t = String.length t in 
    match (len_s, len_t) with 

    (* Bottom cases: the distance is the deletion of the non-empty string. *)
    | (0, _)  -> len_t
    | (_, 0)  -> len_s 
    
    (* Reduce the problem into a smaller size *)
    | (_, _) -> 
      
      (* The cost of matching the character *)
      let d = if( s.[0] = t.[0] ) then 0 else 1 in 

        let s_tail = String.sub s 1 (len_s-1) and 
            t_tail = String.sub t 1 (len_t-1) in

        let del_s  = (levenshtein_distance s_tail t) + 1 and
            del_t  = (levenshtein_distance s t_tail) + 1 and
            del_st = (levenshtein_distance s_tail t_tail) + d in 

        let m1 = min del_s del_t in 
                 min del_st m1 


let diff f1 f2 = 
    let s1_chan = open_in f1 and s2_chan = open_in f2 in
      let s1 = input_line s1_chan and s2 = input_line s2_chan in 
        Printf.printf "%s\n%s\n" s1 s2;
        Printf.printf "Levenshtein Distance: %d\n" 
                      (levenshtein_distance s1 s2); 
        close_in s1_chan;
        close_in s2_chan


let print_usage() =
    print_endline "Usage:";
    print_endline "    diff file1 file2" 


(* The main function *)
 let _ = match Array.length Sys.argv with 
         | 3 -> let seq1 = Sys.argv.(1) and 
                    seq2 = Sys.argv.(2) in 
                diff seq1 seq2;
         | _ -> print_usage() 





































