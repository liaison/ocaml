
exception Out_of_index

let rec k_th_elem_list l k = 
    match (l, k) with 
        | (h::t, 1) -> h 
        | (_, 0)    -> raise Out_of_index
        | ([], _)   -> raise Out_of_index
        | (h::t, k) -> k_th_elem_list t (k-1)  ;;


let _ = 
    let l = [10; 20; 30; 40; 50 ] in 
        let k = 0 in
            try 
            print_int (k_th_elem_list l k); print_newline() 
            with Out_of_index -> print_endline "Out of Index" 

