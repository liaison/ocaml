
(** Imperative features of OCaml *)
let add_polynom p1 p2 = 
    let n1 = Array.length p1 
    and n2 = Array.length p2 in 
        let result = Array.create (max n1 n2) 0 in 
            for i = 0 to n1-1 do result.(i) <- p1.(i) done; 
            for i = 0 to n2-1 do result.(i) <- result.(i) + p2.(i) done; 
            (* The final subexpresion in an expression is the return value*)
            result ;;

let rec add_polynom_fun p1 p2 = 
    match (p1, p2) with 
    | ([], p) -> p 
    | (p, []) -> p 
    | (x::xl, y::yl) -> (x+y)::add_polynom_fun xl yl ;;


add_polynom [| 1; 2 |] [| 1; 2; 3 |] ;;


add_polynom_fun [ 1; 2 ] [ 1; 2; 3 ] ;;

(** Demo of updatable memory cells, called references *)
let fact n = 
    let result = ref 1 in 
        for i = 2 to n do
            result := i * !result 
        done; 
        !result ;; 

fact 5 ;; 



(** Demo for higher-order functions *)
let rec sigma f = function 
    | [] -> 0 
    | x :: l -> f x + sigma f l ;; 


sigma (fun x -> x * x) [1; 2; 3] ;; 
















