

let echo chan = 
    try while true do print_endline (input_line chan) done
    with End_of_file -> () ;;

if Array.length Sys.argv < 2 then echo stdin 
else 
    for i = 1 to Array.length Sys.argv - 1 do
        let chan = open_in Sys.argv.(i) in 
            echo chan; 
            close_in chan 
    done 


